// AGSGalaxy
// GOG Galaxy Plugin for AGS
// Copyright � 2015-2017 MonkeyMoto Productions, Inc.
//
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the LICENSE file for more details.
//
//            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//                    Version 2, December 2004
//
// Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
//
// Everyone is permitted to copy and distribute verbatim or modified
// copies of this license document, and changing it is allowed as long
// as the name is changed.
//
//            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//  0. You just DO WHAT THE FUCK YOU WANT TO.
//
#include <sstream>
#include <string>
#include "AGSGalaxy.h"
#include "galaxy/GalaxyApi.h"
#include "ags2client/agsplugin.h"

static bool GalaxyInitialized = false;

// GOG API helper for user sign-in callbacks
class AuthListener : public galaxy::api::GlobalAuthListener
{
private:
	AuthListener() = default;

public:
	static AuthListener& GetListener() noexcept
	{
		static AuthListener listener{};
		return listener;
	}

	void OnAuthSuccess() noexcept override
	{
		try
		{
			if (galaxy::api::Stats() == nullptr) // this is less than ideal, but will occur if the client is not running or API is not yet initialized
			{
				return;
			}
			// on sign-in, immediately request stats+achievements
			galaxy::api::Stats()->RequestUserStatsAndAchievements();
		}
		catch (...)
		{
		}
	}

	void OnAuthFailure(FailureReason reason) noexcept override
	{
		// TODO: log failure reason?
	}

	void OnAuthLost() noexcept override
	{
	}
};

// GOG API helper for stats+achievements retrieved callbacks
class UserStatsAndAchievementsRetrieveListener : public galaxy::api::GlobalUserStatsAndAchievementsRetrieveListener
{
private:
	UserStatsAndAchievementsRetrieveListener() = default;

public:
	static UserStatsAndAchievementsRetrieveListener& GetListener()
	{
		static UserStatsAndAchievementsRetrieveListener listener{};
		return listener;
	}

	void OnUserStatsAndAchievementsRetrieveSuccess(galaxy::api::GalaxyID userID) noexcept override
	{
		// once we have received the user stats+achievements, we are fully initialized
		GalaxyInitialized = true;
	}

	void OnUserStatsAndAchievementsRetrieveFailure(galaxy::api::GalaxyID userID, FailureReason reason) noexcept override
	{
		// TODO: log failure
	}
};

// Initialize GOG API, sign in the user, and request user stats+achievements (called from user scripts with clientID and clientSecret)
int AGSGalaxy_Initialize(char const *clientID, char const *clientSecret)
{
	if (!GalaxyInitialized)
	{
		try
		{
			galaxy::api::Init(clientID, clientSecret);
		}
		catch (...)
		{
			return false;
		}
		// these CANNOT appear before the API is initialized! d'oh!!!!
		AuthListener &authListener = AuthListener::GetListener();
		UserStatsAndAchievementsRetrieveListener &statsListener = UserStatsAndAchievementsRetrieveListener::GetListener();
		try
		{
			if (galaxy::api::User() == nullptr)
			{
				return false;
			}
			// API is initialized, sign user in (async, see callback classes above)
			galaxy::api::User()->SignIn();
		}
		catch (...)
		{
			return false;
		}
	}
	return GalaxyInitialized;
}

AGSGalaxy::AGSGalaxy& AGSGalaxy::AGSGalaxy::GetAGSGalaxy() noexcept
{
	static AGSGalaxy galaxy{};
	return galaxy;
}

void AGSGalaxy::AGSGalaxy::Startup() const noexcept
{
}

bool AGSGalaxy::AGSGalaxy::IsInitialized() const noexcept
{
	return GalaxyInitialized;
}

void AGSGalaxy::AGSGalaxy::ResetStatsAndAchievements() const noexcept
{
	try
	{
		if (galaxy::api::Stats() == nullptr)
		{
			return;
		}
		galaxy::api::Stats()->ResetStatsAndAchievements();
		// per API docs, this stores changes automatically
	}
	catch (...)
	{
	}
}

char const* AGSGalaxy::AGSGalaxy::GetExtraFunctionsForScriptHeader() const noexcept
{
	return
		"  ///AGSGalaxy: Attempts to initialize the Galaxy client with the specified ID and secret. Must be called before Galaxy features can be used. Initialization is asynchronous and may not occur immediately.\r\n"
		"  import static void Initialize(const string galaxyClientID, const string galaxyClientSecret);\r\n";
}

char const* AGSGalaxy::AGSGalaxy::GetUserName() const noexcept
{
	try
	{
		if (galaxy::api::Friends() == nullptr)
		{
			return nullptr;
		}
		char const *name = galaxy::api::Friends()->GetPersonaName();
		return (name == nullptr ? nullptr : name);
	}
	catch (...)
	{
		return nullptr;
	}
	return nullptr;
}

void AGSGalaxy::AGSGalaxy::Shutdown() const noexcept
{
	try
	{
		galaxy::api::Shutdown();
	}
	catch (...)
	{
	}
}

void AGSGalaxy::AGSGalaxy::Update() const noexcept
{
	try
	{
		galaxy::api::ProcessData();
	}
	catch (...)
	{
	}
}

char const* AGSGalaxy::AGSGalaxy::GetAGSPluginName() const noexcept
{
	return "AGSGalaxy";
}

char const* AGSGalaxy::AGSGalaxy::GetAGSPluginDesc() const noexcept
{
	return "AGSGalaxy: GOG Galaxy Plugin for AGS (C) 2015-2017 MonkeyMoto Productions, Inc.";
}

char const* AGSGalaxy::AGSGalaxy::GetCurrentGameLanguage() const noexcept
{
    try
    {
        return galaxy::api::Apps()->GetCurrentGameLanguage();
    }
    catch (...)
    {
    }
}

float AGSGalaxy::AGSGalaxy::GetVersion() const noexcept
{
	return AGSGalaxy::VERSION;
}

bool AGSGalaxy::AGSGalaxy::ClaimKeyPress(int, int(*)(int)) const noexcept
{
	// TODO: GOG supported feature? (Shift+Tab code below)
	//bool isShift = ((data == 403) || (data == 404)); // is pressed key shift
	//bool isTab = (data == 9); // is pressed key tab
	//bool isShiftTab = ((data == 0x00001111) || // shift+tab as a single key
	//	((isShift) && (IsKeyPressed(9) != 0)) || // key is shift and tab is held
	//	((isTab) && ((IsKeyPressed(403) != 0) || (IsKeyPressed(404) != 0)))); // key is tab and shift is held
	//return isShiftTab; // Claim (Shift+Tab), ignore other keys
	return false;
}

void AGSGalaxy::AGSGalaxy::RegisterScriptFunctions(IAGSEngine *engine) const noexcept
{
	static std::string initialize = std::string{ this->GetClientNameForScript() } + "::Initialize^2";
	IAGS2Client::RegisterScriptFunctions(engine);
	engine->RegisterScriptFunction(initialize.c_str(), reinterpret_cast<void*>(AGSGalaxy_Initialize));
}
