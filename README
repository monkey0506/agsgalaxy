AGSGalaxy
GOG Galaxy Plugin for AGS
Copyright � 2015-2016 MonkeyMoto Productions, Inc.

Visual Studio build files are located in the "Solutions" folder. Linux
project files for Code::Blocks will be added at a later time.

v1.0 - 08 January 2016

The AGSGalaxy plugin now supports "unified" building by default. This
means that you will use "AGS2Client" in place of "AGSGalaxy" in most
cases. A call such as:

	AGSGalaxy.SetAchievementAchieved("NAME");

Will instead become:

	AGS2Client.SetAchievementAchieved("NAME");

The benefit behind this is that you can toggle between AGSGalaxy and
AGSteam (or other future plugins) without having to change your game
scripts (assuming the same API name is used for achievements on both
GOG Galaxy and Steamworks).

You may instead choose to utilize the "disjoint" build of the plugin,
which still uses "AGSGalaxy" instead of "AGS2Client", but this means
you will have to use separate code for Steam builds. A practical
reason why you might want to do this is to allow "Galaxy crossplay"
builds. Essentially that means that you would include BOTH the
AGSGalaxy AND AGSteam disjoint builds AT THE SAME TIME. You would
initialize both client APIs, and call their respective functions
separately. Doing so would allow you to have a single build of your
game released on Steam and GOG Galaxy. As long as the player has
both clients installed, then they can gain achievements for both
clients at once.

Additionally, the AGSGalaxy plugin now fully supports Stats and
Leaderboards, which were missing from the initial release.




v0.5 - 04 January 2016

The AGSGalaxy plugin currently supports setting GOG Galaxy achievements
from within your AGS game scripts. Stats and Leaderboards are not
currently supported.

To use the plugin, make sure it is enabled for your project in the AGS
editor, make sure the GOG Galaxy client is running and you are logged
in. In your game_start function, initialize the plugin like this:

	String CLIENT_ID = "YOUR_CLIENT_ID_HERE";
	String CLIENT_SECRET = "YOUR_CLIENT_SECRET_HERE";
	AGSGalaxy.Initialize(CLIENT_ID, CLIENT_SECRET);

Note that initialization is an asynchronous process, and may not finish
immediately. You can check to make sure that initialization is complete
using the AGSGalaxy.Initialized property (boolean). Then you can set
achievements using the SetAchievementAchieved function:

	AGSGalaxy.SetAchievementAchieved("YOUR_ACHIEVEMENT_HERE");

You can also check if an achievement is set using IsAchievementAchieved:

	Display("Achieved? %d", AGSGalaxy.IsAchievementAchieved("YOUR_ACHIEVEMENT_HERE");

Additional features will be added at a later time.